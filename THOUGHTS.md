It`s a pretty common task to implement data tree with mentioned requirements (for me at least)

Library should be a reference to own implementation, but it depends on goal.
If we need MVP - use library
If we need long term maintenance app, it's better to use own impl, to be independent and free of library limits

As it was mentioned - react or vue, nobody knows
That's why I should look for pure js/ts lib

---

<h2>About requirements:</h2>

>Scalable for big data set

Virtual scroll will help with it

---

<h2>Summary about libraries</h2>

I decided to pick one pure library, and one for each framework we spoke.

Every library have one common con - some limitation that will certainly appear some day.
Although pure variant has only basic tree render support. And it's only limitation - dom structure it provides.

I won't suggest any of these :)<br>
At least for "long term".
Ready for framework variants may be used for MVP.
