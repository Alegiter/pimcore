Lib - https://www.npmjs.com/package/virtual-scrolling-tree?activeTab=readme

pros:
- designed to be easily wrapped by third-party frameworks (as docs says)
- can handle big data sets
- by default rerenders all rows, but has a option to rerender only updating rows
- callback for item render
  - can render any template I want
  - can be used to impl "Show some information on demand"
- supports async loading
- useful inbuilt methods: expand, collapse, scrollIntoView
- zero dependencies

cons:
- too many features to impl by my own
- no repo to look through original code
  - no support apparently
- not native scrolling
  - has a smoothScrolling options, but it can affect on async children loading negatively 

From the first glans it's unclear how it destroys item and how to do "unsubscribe" stuff