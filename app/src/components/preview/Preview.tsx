import { memo, useEffect } from "react"
import { useStore } from 'react-context-hook'
import { Store } from "../../common/store/store"

export const Preview = memo(function Preview() {
    const [previewId] = useStore<Store["previewId"]>("previewId")

    const dataAfterLoad = `Preview for ${previewId}`

    // Actually it's a bad approach to fetch data from useEffect, but the idea
    useEffect(() => {
        // init fetch smth for new previewId
        return () => {
            // unsubscribe fetch for previous previewId
        }
    }, [previewId])

    return (
        <div>
            {dataAfterLoad}
        </div>
    )
})
