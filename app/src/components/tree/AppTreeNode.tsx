import { memo, useCallback, useState } from "react"
import { MyTypeItem } from "../../common/model/type"
import { useStore } from 'react-context-hook'


type Props = MyTypeItem

export const AppTreeNode = memo(function AppTreeNode(props: Props) {
    const { id, itemData, toggle, isOpen, isLeaf } = props

    const [, setPreviewId, deletePreviewId] = useStore("previewId")

    const [isLoadingChildren, setIsLoadingChildren] = useState(false)

    const onClick = useCallback(() => {
        setIsLoadingChildren(true)

        load(id)
            .then(() => {
                toggle(id)
            })
            .finally(() => {
                setIsLoadingChildren(false)
            })
    }, [id, toggle])

    return (
        <div key={id}
            onClick={onClick}
            onMouseEnter={() => setPreviewId(id)}
            onMouseLeave={deletePreviewId}
        >
            {!isLoadingChildren && isLeaf === false && (
                <span>
                    {isOpen ? "+" : "-"}
                </span>
            )}
            {isLoadingChildren && (
                <span>
                    @
                </span>
            )}
            <span>
                {itemData.label}
            </span>
        </div>
    )
})

function load(id: string): Promise<void> {
    return new Promise((resolve) => {
        setTimeout(() => {
            // Fetch by id
            // Update treeData in store

            // It would be definitely easier if use flat tree data approach
            // Then I just need to find index by id
            // and splice with insert children from that index

            // As for nested approach
            // Recursive search by id for example

            resolve()
        }, 1000)
    })
 }