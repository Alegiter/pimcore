import { memo } from "react"
import { Tree } from "@quantum_box/react"
import { MyType } from "../../common/model/type"
import { AppTreeNode } from "./AppTreeNode"

type Props = {
    data: MyType
}

export const AppTree = memo(function AppTree(props: Props) {
    const { data } = props
    
    return (
        <Tree
            containerHeight={600}
            itemHeight={44}
            indent={20}
            data={data}
            Item={AppTreeNode}
        />
    )
})