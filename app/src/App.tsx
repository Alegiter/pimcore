import './App.css';
import { MyType } from './common/model/type'
import { useStore, withStore, store } from 'react-context-hook'
import { Preview } from './components/preview/Preview'
import { AppTree } from './components/tree/AppTree'

function App() {
  const [treeData] = useStore<MyType>("treeData")
  return (
    <div className="App">
      {!treeData && "Loading..."}
      {treeData && <AppTree data={treeData} />}
      <Preview/>
    </div>
  );
}

export default withStore(App)

// Init store some how, it's not important now
const data: Array<MyType> = []
for (let i = 0; i < 10000; i++) {
  data.push({
    id: i,
    value: {
      label: `node-${i}`,
      description: `Description for node-${i}`
    },
    children: [],
  })
}
store.set("treeData", {
  id: "root",
  value: {
    label: "root",
    description: "root desc"
  },
  children: data
})