import { RQBTree } from "@quantum_box/react"

export type DataObject<ValueType> = RQBTree.DataObject & {
    value: ValueType
}

export type MyType = DataObject<{
    label: string
    description: string
}>

export type Item<ItemDataType> = RQBTree.Item & {
    id: string
    itemData: ItemDataType
}

export type MyTypeItem = Item<{
    label: string
    description: string
}>